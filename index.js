
let studentName = [];

//3
function addStudent(name){
	studentName.push(name);
	console.log(`${name} is added to the student's list.`)
}
//4
function countStudents(){
	console.log(`There are a total of ${studentName.length} students enrolled.`);
}
//5
function printStudents(){
	studentName.sort().forEach(function(name){
	console.log(name);
	})
}

//6
function findStudent(criteria){
	let criteriaLower = criteria.toLowerCase();
	let resultArray = studentName.filter(function(val){
		return val.toLowerCase().includes(criteriaLower);
	});
	if (resultArray.length==0){
		console.log(`No student found with the name ${criteria}`);
	}
	else if (resultArray.length==1) {
		console.log(`${resultArray.toString()} is an enrollee.`);
	}else{
		console.log(resultArray.toString()+" are enrollees.");
	}
}